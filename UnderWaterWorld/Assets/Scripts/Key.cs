using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour, IKey //inherit from the key interface IKey (alt+Enter intellisense menu and 'implement interface' auto generates required methods, if red line click and use quick action refactoring to fix)
{
    public enum KeyType { a,b } //enum data type that can be read as a string or numerical value, a and b for two keys

    public KeyType type; //variable of enum type
    [TextArea] //attribute starts with a square bracket [] able to modify text box size
    public string tooltipText; //able to modify tooltip text in inspector
    public AudioClip pickupClip; //assign audio clip in inspector

    private AudioSource audioSource;

    void Start()
    {
        audioSource = GameManager.Instance.GetComponent<AudioSource>();
        WorldMap.Instance.AddInteraction(gameObject); //add key to the world map interaction using AddInteraction function pass gameObject through
    }

    void Update()
    {
        
    }
    /// <summary>
    /// Key interaction, add key if able to be picked up, play key audio and remove key from scene
    /// </summary>
    public void Pickup()
    {
        if (Interaction.Instance.AddKey(type) == true) //get access to the key through the singleton, pass KeyType through method, checks if key is successfully picked up
        {
            audioSource.PlayOneShot(pickupClip);
            gameObject.SetActive(false); //turn object off, want interactable key to despawn in scene
        }        
    }
    /// <summary>
    /// Display tool tip text when inspecting the key.
    /// </summary>
    /// <returns></returns>
    public string Inspect() //able to use the method for all interactions
    {
        return tooltipText; 
    }
}
