using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class Gate : MonoBehaviour, IGate
{
    [TextArea]
    public string tooltipText;
    public Key.KeyType type; //gates are locked with specific keys, need to specify type of key to unlock
    public PlayableAsset cutscene;
    public GameObject barrier;
    public GameObject doorbarrier;
    public GameObject wallbarrier;

    public bool Locked { get; private set; } = true; //only want to set within the Gate script, can publically get it and privately set it, default value of true, all gates will be locked by default

/// <summary>
/// Add Instance of a gate to show on world map
/// </summary>
    void Start()
    {
        WorldMap.Instance.AddInteraction(gameObject); //required to show gate on World Map, adds to World Map
    }

    /// <summary>
    /// Show tool tip text when gate is inspected
    /// </summary>
    /// <returns></returns>
    public string Inspect()
    {
        return tooltipText;
    }
    /// <summary>
    /// If the gate is locked and the player has a key then unlock gate and turn off barriers
    /// </summary>
    /// <returns></returns>
    public bool Unlock()
    {
        if(Locked == true)
        {
            if(Interaction.Instance.HasKey(type) == true) //get access to Interaction through singleton, HasKey method pass through Type, if it is true (HasKey)
            {
                Locked = false;
                GameManager.Instance.TriggerCutscene(cutscene);
                barrier.SetActive(false);
                doorbarrier.SetActive(false);
                wallbarrier.SetActive(false);
                Debug.Log("Cutscene " + type + " has been triggered");
                return true;
            }
            //else no gate key collected
            else
            {
                //Tooltip "You are missing the key" message
                Debug.Log("Key is missing");
            }
        }
        //else gate is already unlocked
        else
        {
            //Tooltip "already unlocked" message
            Debug.Log("already unlocked");
        }
        return false; //use return false to get rid of error statement
    }

    void Update()
    {
        
    }
}
