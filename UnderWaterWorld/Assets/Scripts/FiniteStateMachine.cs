//starts with blank script

namespace FiniteStateMachine
{
    public interface IState //state interface IState template with three states
    {
        void OnEnter(); //OnEnter function called when state first entered

        void OnUpdate(); //OnEnter function called when state executed

        void OnExit(); //OnExit function called when right before state finishes executing
    }

    public class StateMachine //template to create variables of the type
    {
        public IState CurrentState { get; private set; } //read publicly and write privately

        //two constructors for the class
        public StateMachine() //blank constructor for StateMachine
        {
        }

        public StateMachine(IState initState) //constructor takes in the intial state, when initializing will start in a particular state
        {
            SetState(initState); //call SetState function initial state initState
        }

        /// <summary>
        /// If the CurrentState is not equal to null OnExit then set the CurrentState to a newState OnEnter.
        /// </summary>
        /// <param name="newState"></param>
        
        public void SetState(IState newState) //call SetState function pass newState to change into
        {
            if (CurrentState != null) //before newState check Current State is not equal to null
            {
                CurrentState.OnExit(); //call OnExit if not equal to null
            }
            CurrentState = newState; //call current state new state
            CurrentState.OnEnter();
        }
       
        public void OnUpdate() 
        {
            if (CurrentState != null) //every frame check CurrentState is not equal to null
            {
                CurrentState.OnUpdate(); //if not equal to null call the CurrentState OnUpdate
            }
        }

        public StateType GetCurrentStateAsType<StateType>() where StateType : class, IState //where add constraint of StateType inheriting from class IState
        {
            return CurrentState as StateType;
        }
    }
}