using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Interaction script attached to the camera

public class Interaction : MonoBehaviour
{
    public float distance = 2; //distance infront of camera that the player can interect with things, defult value of 2

    public static Interaction Instance { get; private set; } //singleton

    private List<Key.KeyType> keyRing = new List<Key.KeyType>(); //keyring variable, declare a list once keytype enum variable created (Key.cs), list is of the KeyType, KeyType is encapsulated in another class, need to access it as its defined inside Key need to have as Key.KeyType to recognise it

    //no Start function needed 

    private void Awake()
    {
        if(Instance == null) //set up singleton
        {
            Instance = this;
        }
        else
        {
            enabled = false;
        }
    }

    void Update()
    {
        if(InteractionMenu.Instance.gameObject.activeSelf == false) //if the interaction menu is not open
        {
            if (ActionMenu.Instance.gameObject.activeSelf == false && WorldMap.Instance.legend.gameObject.activeSelf == false) //if action menu and map are not open then execute the following
            {
                if (Input.GetButtonDown("Interaction") == true) //GetButtonDown return on the frame if the button has been pressed (true)
                {
                    if (Physics.SphereCast(transform.position, 0.5f, transform.forward, out RaycastHit hit, distance) == true) //if the spherecast hits something, can also use ray casting, calculates postion ray is cast from (camera in this instance), want forward direction of object
                    {
                        if (hit.collider.TryGetComponent(out IInteraction interaction) == true) //check hit for interaction type if spherecast hits something
                        {
                            Debug.DrawRay(transform.position, transform.forward * distance, Color.green, 0.25f); //draw rays in scene for debugging
                            InteractionMenu.Instance.Activate(interaction); //activate interaction
                        }
                    }
                }
            }
        }
        else //else if the interaction menu is open check if interact button is released
        {
            if(Input.GetButtonUp("Interaction") == true)
            {
                InteractionMenu.Instance.Deactivate();
            }
        }
    }
    public bool AddKey(Key.KeyType key) //AddKey is a method to check if key is added, can't pick up the same type of key more than once, each key has a specific interaction 
    {
        if (keyRing.Contains(key) == false) //check if keyring already contains keytype
        {
            keyRing.Add(key); //if keyring does not contain key add the key
            return true;
        }
        return false;
       
    }
    public bool HasKey(Key.KeyType key) //HasKey is a method used to check if already have key
    {
        return keyRing.Contains(key);
    }
}

//under Interaction class, create a base or abstract interface, acts as the base level of an Interaction
//only contains the methods that apply to every single interaction in the game

public interface IInteraction //interface
{
    string Inspect(); //Inspect fuction returns a string from where is called from (not a void)
    //as it is an interface the funtionality of the method isn't defined, like saying this interface includes this method
    //the method can be redefined but has to be called Inspect, with no parameters and return a string
}

public interface IFauna : IInteraction //IFauna repsresents all Fish and inherits (using the colon) IIteraction
    //IFaunna has the Inspect function as it inherits from IInteraction
{
    void Photograph(string folderPath = null); //void Photograph, parameter is an optional parameter and therefore equal to null
}

public interface IKey : IInteraction //IKey inherits from IInteraction (able to Inspect)
{
    void Pickup(); //void Pickup key
}

public interface IGate : IInteraction //IGate inherits from IInteraction (able to Inspect)
{
    bool Unlock(); //gate returns a boolean called UnLock, gate is either locked or unlocked
}

public interface IShip : IInteraction //IShip inherits from IInteraction (able to Inspect)
{
    void Leave();
}