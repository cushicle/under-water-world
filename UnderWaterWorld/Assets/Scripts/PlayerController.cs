using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //declare variables for controller
    public float speed = 3f; //move speed of controller set to 3f

    private Vector3 input; //store input (0,0,0)
    private Vector3 motion; //store motion (0,0,0)
    private CharacterController controller; //reference to character controller to apply movement

    public static PlayerController Instance { get; private set; } //singleton

    private void Awake() //Awake function best used to set all script references before they get used, Start is for the first frame
    {
        //singleton setup
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            enabled = false;
        }
        controller = GetComponent<CharacterController>(); //reference the character controller
    }

    void Update() //Update goes every frame, get recalcuated each time
    {
        //get motion and apply to character
        motion = Vector3.zero; //reset Vector 3(0,0,0) refreshing motion
        input = new Vector3(Input.GetAxisRaw("Horizontal"), //new Vector 3 (x,y,z) value, x value is horizontal
            Input.GetAxisRaw("Depth"), //y value is depth
            Input.GetAxisRaw("Vertical")); //z value is vertical, get inputs, compiles in one line due to semi colon
        motion += transform.forward.normalized * input.z; //calculates forward motion, normalized prevents z and x axis from adding together when holding down two directions on controller, transform.forward relative to object attached to in local space (not world space)
        motion += transform.right.normalized * input.x; //calculates horizontal motion, use normalized, tranform.right is relative to object script is attached to, local space (not world space)
        motion += Vector3.up.normalized * input.y; //calculates vertical motion, use normalized (maybe not needed?), Vector3.up gets up direction in World Space not local space, never changes always points up
        controller.Move(motion * speed * Time.deltaTime); //apply movement according to inputs pass motion vector multiplied by speed multiplied by the time it takes to get from one frame to another (Time.deltaTime), creates a minute value that is applied to a small amount of movement
    }

    public void MoveToPosition(Vector3 targetPos)
    {
        controller.enabled = false; 
        transform.position = targetPos;
        controller.enabled = true;
    }
}
