using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlockManager : MonoBehaviour
{
    /// <summary>
    /// Set flocking behaviours, distance settings and weights in Unity Inspector
    /// </summary>
    [Header("Flock Setup")]
    public FlockAgent agentPrefab;
    public int size;
    public Vector3 spawnBounds;
    public Vector2 speed;
    [Header("Distance Settings")]
    [Range(0, 100)] public float cohesionDistance;  //distance settings range has a min of 0 and max of 100
    [Range(0, 100)] public float separationDistance;
    [Range(0, 100)] public float alignmentDistance;
    [Range(0, 100)] public float boundsDistance;
    [Range(0, 100)] public float obstacleDistance;
    [Header("Weights")]                             //weights settings range has a min of 0 and max of 100
    [Range(0, 100)] public float cohesion;
    [Range(0, 100)] public float separation;
    [Range(0, 100)] public float alignment;
    [Range(0, 100)] public float bounds;
    [Range(0, 100)] public float obstacleAvoidance;

    public FlockAgent[] SpawnedAgents { get; private set; }


  /// <summary>
  /// Spawns flock agents on first frame of the game
  /// </summary>
    void Start()
    {
        SpawnAgents(); //spawn agents into game during start frame or first frame
    }

    /// <summary>
    /// Updates each flock agent and increments the value of i 1 until length reached
    /// </summary>
    void Update()
    {
        for (int i = 0; i < SpawnedAgents.Length; i++) //for loop iterates through spawned agents and increments frame to frame
        {
            SpawnedAgents[i].Move(); //spawned agents move method
        }
    }

    /// <summary>
    /// This method is used to spawn the flock agents into the world.
    /// </summary>
    void SpawnAgents()
    {
        SpawnedAgents = new FlockAgent[size];
        for (int i = 0; i < size; i++) //for loop iterates through FlockAgent size array and increments frame to frame until max flock size reached
        {
            Vector3 random = Random.insideUnitSphere; //Random.insideUnitSphere, returns a random point inside or a on a sphere with a radius 1.0
            random = new Vector3(random.x * spawnBounds.x, random.y * spawnBounds.y, random.z * spawnBounds.z);
            Vector3 spawnPos = transform.position + random; //spawn agents into a random poistion
            Quaternion rot = Quaternion.Euler(0, Random.Range(0, 360), 0); //quarternion 4d position(x,y,z,w), represents rotations based on complex numbers
            SpawnedAgents[i] = Instantiate(agentPrefab, spawnPos, rot); //Instantiate function makes a copy of an object, it clones the object original and returns a clone
            SpawnedAgents[i].Initialize(this, Random.Range(speed.x, speed.y)); //Initialize function, compiler initializes the spawned agents using Flock Manager at a random speed within a min max range
        }
    }
    /// <summary>
    /// Draw red gizmo to indicate spawnBounds for flock
    /// </summary>
    private void OnDrawGizmos() //debugging
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position, spawnBounds); //draw gizmo to show the spawn bounds of the flock
    }
}
