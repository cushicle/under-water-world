using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//prefab objects are a template in the project, don't exist in the scene
//Instantiate, method for spawning in new objects in Unity

public class Actions : MonoBehaviour
{
    //wouldn't normally create public variables easy to reverse, only used for teaching purposes to show in Unity Inspector
    //best to use private and [SerializedField] to stop someone changing the variables through reverse engineering.
    //no classes need access to them
    //variables for Actions
    public float boostMultipler = 5; //could list all floats in one line
    public float boostTime = 3;
    public float sonarTime = 3;
    public float sonarRadius = 15;
    public int iconLimit = 5;
    public LayerMask interactionLayer; //LayerMask?
    public GameObject sonarIconPrefab;
    public static Actions Instance { get; private set; }

    private float sonarTimer = -1; //timer for sonar
    private float boostTimer = -1; //timer for boost
    private List<GameObject> spawnedSonarIcons = new List<GameObject>(); //private list of GameObjects that is spawned sonar icon and set to a new empty list

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            enabled = false;
        }
    }

    void Start()
    {
        for (int i = 0; i < iconLimit; i++) //for loop to execute
        {
            spawnedSonarIcons.Add(Instantiate(sonarIconPrefab, ActionMenu.Instance.transform.root, false));
            //Instantiate, spawn new objects into Unity (object, transform, rotation)
            //...parameters,sonarIconPrefab is the orginal object, parents of sonar objects spawned in(Action Menu), make canvas parent root object (uppermost parent of object)
            //...convert to a transform to get root object, false don't spawn as world position changes
            spawnedSonarIcons[i].transform.position = Vector3.zero; //access element at index i, set position to middle of screen
            spawnedSonarIcons[i].transform.SetAsFirstSibling(); //important for draw order on screen
            spawnedSonarIcons[i].SetActive(false); //turn sonar icons on off
        }
        //Deactivate Map
        Map(false);
    }
    void Update()
    {
        //if the world map is not active
        if (WorldMap.Instance.legend.activeSelf == false)
        {
            if (ActionMenu.Instance.gameObject.activeSelf == false)
            {
                if (InteractionMenu.Instance.gameObject.activeSelf == false)
                {
                    if (Input.GetButtonDown("Action") == true)
                    {
                        ActionMenu.Instance.Activate();
                    }
                }
            }
            else
            {
                if (Input.GetButtonUp("Action") == true)
                {
                    int selection = ActionMenu.Instance.Deactivate();
                    switch (selection)
                    {
                        case 0:
                            Boost(true);
                            break;
                        case 1:
                            Sonar(true);
                            break;
                        case 2:
                            Map(true);
                            break;
                    }
                }
            }
        }
        else if (Input.GetButtonDown("Action") == true) //else if get button down "action"
        {
            Map(false); //turn off map
        }

        //boost timer
        if (boostTimer > -1)
        {
            boostTimer += Time.deltaTime;
            if (boostTimer > boostTime)
            {
                Boost(false); //deactivate boost
            }
        }

        if (sonarTimer > -1)
        {
            Collider[] closeInteractions = Physics.OverlapSphere(transform.position, sonarRadius, interactionLayer); //local variable an array of colliders, Physics.OverlapSphere (part of a LayerMask), position centre of sphere to find everything within a radius
            int interactionCount = closeInteractions.Length; //Length is arrays...slower, count is lists...quicker
            for (int i = 0; i < iconLimit; i++) //for loop starts to cycle, i = 0 start from beginning
            {
                if (i < interactionCount) //check i is less than interaction count, example if only 3 interactions around and 5 icons total, only execute code when there are icons available
                {
                    Vector3 screenPos = Camera.main.WorldToScreenPoint(closeInteractions[i].transform.position); //screenPos is 2d stored in Vector3 (3d), turns into a screen space value
                    if (screenPos.z > 0) //can use depth(z) to see on screen, if object is visible on screen (negative z means object is not on screen)
                    {
                        spawnedSonarIcons[i].transform.position = screenPos; //use i counter
                        if (spawnedSonarIcons[i].activeSelf == false) //check if inactive
                        {
                            spawnedSonarIcons[i].SetActive(true); //activate sonar icons
                        }
                    }
                    else if (spawnedSonarIcons[i].activeSelf == true) //if icon is active and not on screen
                    {
                        spawnedSonarIcons[i].SetActive(false); //turn off icon
                    }
                }
                else if (spawnedSonarIcons[i].activeSelf == true) //if icon is active but no corresponding interaction
                {
                    spawnedSonarIcons[i].SetActive(false); //turn off icon
                }
            }
            sonarTimer += Time.deltaTime; //sonar timer functionality
            if (sonarTimer > sonarTime) //if sonarTimer greater than sonarTime
            {
                Sonar(false); //deactivate sonar
            }
        }
    }
    void Boost(bool toggle)
    {
        if (toggle == true) //pass true turn on the functionality
        {
            if (boostTimer == -1) //check to see already not boosting, check boost timer off
            {
                boostTimer = 0; //if the boost timer is off, equal to 0 then turn boost timer on
                PlayerController.Instance.speed *= boostMultipler; //to increase speed of player controller not only one player, use as a singleton, access speed variable, speed increased by boostMultiplier (eg. 5 x faster)
            }
        }
        else  //pass false turn off the functionality
        {
            if (boostTimer > -1) //if boost timer off, needs this check to stop boost from being cancelled when opened in world map even when not on.
            {
                boostTimer = -1;
                PlayerController.Instance.speed /= boostMultipler; //decrease speed of player controller by dividing boostMultiplier, divide current speed
            }
        }
    }

    void Sonar(bool toggle)
    {
        if (toggle == true) //activate sonar
        {
            if (sonarTimer == -1) //only activate sonar if it's not already activated
            {
                sonarTimer = 0; //set sonar timer to 0 to start counting
            }
        }
        else // deactivate sonar
        {
            sonarTimer = -1; //turn sonar timer off
            if (spawnedSonarIcons.Count > 0) //check there is some spawned icons
            {
                foreach (GameObject go in spawnedSonarIcons) //loop through each of the spawnSonarIcons
                {
                    go.SetActive(false); //turn off spawnedSonarIcons
                }
            }
        }
    }

    void Map(bool toggle)
    {
        if (toggle == true) //activate Map
        {
            PlayerController.Instance.enabled = false; //turn off player movement
            MouseLook.Instance.enabled = false; //turn off mouse look
            Interaction.Instance.enabled = false; //turn off interaction
            Sonar(false); //deactivate sonar
            Boost(false); //deactivate boost
            WorldMap.Instance.Activate(); //activate map
        }
        else //deactivate Map
        {
            if (GameManager.Instance.cutSceneCamera.gameObject.activeSelf != true)
            {
                PlayerController.Instance.enabled = true; //turn on player movement
                MouseLook.Instance.enabled = true; //turn on mouse look
                Interaction.Instance.enabled = true; //turn on interaction
            }
            WorldMap.Instance.Deactivate(); //deactivate map
        }
    }
}