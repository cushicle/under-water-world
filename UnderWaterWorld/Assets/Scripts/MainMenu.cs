using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class MainMenu : MonoBehaviour
{
    public GameObject galleryPanel;
    public GameObject infoPanel;

    private Dropdown galleryDropdown;
    private RawImage galleryDisplay;
    private List<Texture2D> screenshots = new List<Texture2D>();

    private void Awake()
    {

        galleryDropdown = galleryPanel.GetComponentInChildren<Dropdown>();
        galleryDisplay = galleryPanel.GetComponentInChildren<RawImage>();
        galleryDropdown.ClearOptions();

        #region Confirm Gallery Folder Exists
        //check for gallery folder, create if does not already exist
        string galleryFolder = Application.dataPath + "/Resources/PhotoGallery";
        if (Directory.Exists(galleryFolder) == false)
        {
            Directory.CreateDirectory(galleryFolder);
        }
        #endregion

        DirectoryInfo dir = new DirectoryInfo(galleryFolder);
        FileInfo[] screenshotInfos = dir.GetFiles("UWW_?*.png");
        if(screenshotInfos.Length > 0)
        {
            foreach (FileInfo info in screenshotInfos) //for each loop search through each FileInfo in screenshotInfos
            {
                galleryDropdown.options.Add(new Dropdown.OptionData(info.Name)); //add drop down option for file
                Texture2D texture = new Texture2D(960, 540); //create a new texture for the screenshot
                texture.LoadImage(File.ReadAllBytes(info.FullName)); //load image from the file as a texture
                screenshots.Add(texture); //add texture to list of screenshots
            }
            galleryDisplay.texture = screenshots[0];
        }
        else
        {
            galleryDisplay.gameObject.SetActive(false);
        }
    }

    void Start()
    {
        galleryPanel.SetActive(false); 
        infoPanel.SetActive(false);
    }

    IEnumerator LoadGame() //IEnumerator for fade in
    {
        yield return new WaitForSeconds(2); 
        SceneManager.LoadScene(1);
    }

    public void StartGame() 
    {
        StartCoroutine(LoadGame());
    }
    /// <summary>
    /// Show info panel 
    /// </summary>
    public void Info() 
    {
        if (galleryPanel.activeSelf == true) //if the gallery panel is on
        {
            galleryPanel.SetActive(false); //turn the gallery panel off
        }
        infoPanel.SetActive(!infoPanel.activeSelf); //turn the info panel on, using activeSelf
    }

    /// <summary>
    /// Show gallery panel
    /// </summary>
    public void Gallery() 
    {
        if (infoPanel.activeSelf == true) //if the info panel is on
        {
            infoPanel.SetActive(false); //turn the info panel off
        }
        galleryPanel.SetActive(!galleryPanel.activeSelf); //turn the gallery panel on, using activeSelf
    }
    /// <summary>
    /// Show gallery drop down menu
    /// </summary>
    public void SelectGalleryImage()
    {
        if (screenshots.Count > 0) //if there is 1 or more screenshots, check if there is more screenshots
        {
            if (galleryDisplay.gameObject.activeSelf == false) //check if the gallery display is off, gallery image automatically turned off if there is no screenshots that exist
            {
                galleryDisplay.gameObject.SetActive(true); //if the gallery display is off, turn it on
            }
            galleryDisplay.texture = screenshots[galleryDropdown.value]; //set the texture equal to the array of screenshots, index passed from the gallery dropdown list
        }
    }

    public void QuitToDesktop() 
    {
        Application.Quit();
    }
}