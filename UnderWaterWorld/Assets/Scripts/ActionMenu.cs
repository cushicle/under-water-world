using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; //Inherit from the UI space

public class ActionMenu : MonoBehaviour
{
    public Image[] menuOptions; //array of images for menu

    public static ActionMenu Instance { get; private set; } //singleton of ActionMenu

    private int selection = -1; //for currently selected option
    private float timer = -1; //float for timer

    private void Awake() //use Awake function to create singleton
    {
        //initialize singleton
        if(Instance == null) //if the Instance of the Action is equal to null
        {
            Instance = this; //Action Menu enabled
        }
        else
        {
            enabled = false; //else Action Menu is not showing
        }
    }
    void Start()
    {
        Deactivate();
    }
  
    void Update()
    {
        if(gameObject.activeSelf == true && timer == -1) //if the Action Menu gameObject boolean is equal to true and the timer is off.
        {
            int axis = Mathf.FloorToInt(Input.GetAxis("Menu Selection")); //use Mathf to Floor,FloorToInt to get lowest number possible to round to eg. 5.6 round to 5. CeilToInt would return 6, highest number to round to possible.
            Debug.Log(axis);
            if(axis != 0)
            {
                timer = 0;
                selection += axis;
                if(selection < 0)
                {
                    selection = menuOptions.Length - 1;
                }
                else if(selection > menuOptions.Length - 1)
                {
                    selection = 0;
                }
                UpdateUISelection();
            }
        }

        //timer functionality
        if(timer > -1) //if timer is on
        {
            timer += Time.deltaTime;//if the timer is ticking
            if(timer > 0.2f) //if the timer is larger than the UI buffer, 0.2f has been hardcoded as a buffer
            {
                timer = -1; //turn timer back off
            }
        }
    }
    /// <summary>
    /// Action menu selected and unselected color
    /// </summary>
    void UpdateUISelection() //gets called everytime need to update menu UI
    {
        Color selectedColor;
        Color unselectedColor;
        ColorUtility.TryParseHtmlString("#EDF500", out selectedColor); //ColorUtlity, using TryParseHtmlString method to convert html color string for selected color
        ColorUtility.TryParseHtmlString("#EDF500", out unselectedColor); //ColorUtlity, using TryParseHtmlString method to convert html color string for unselected color

        for (int i = 0; i < menuOptions.Length; i++) 
        {
            if (i == selection) //if i is equal to the selection variable, or currently selected option
            {
                menuOptions[i].color = selectedColor; //access the menu image from the array, menu shows selected html color
            }
            else
            {
                menuOptions[i].color = Color.grey; //else menu shows grey
            }
        }
    }
    
    public void Activate() 
    {
        gameObject.SetActive(true); //turn menu on
        selection = 0; //set selection to 0
        UpdateUISelection(); //update UI
    }

    public int Deactivate() //int used instead of void, needs to return an integer
    {
        gameObject.SetActive(false); //set game object off
        int s = selection; //create a new integer called s to store selection
        selection = -1; //set selection to -1
        return s; //then return s
    }
}
