using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempInteraction : MonoBehaviour, IShip
{
    private void Start()
    {
        WorldMap.Instance.AddInteraction(gameObject);
    }
    public string Inspect()
    {
        return "Fish";
    }

    public void Leave()
    {
        Debug.Log("Leaving");
    }
}