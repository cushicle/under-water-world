using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tooltip : MonoBehaviour
{
    public float tooltipTime = 5;

    private Text tooltipText;
    private float timer = -1;

    public static Tooltip Instance { get; private set; } //singleton
    
    private void Awake()
    {
        if(Instance == null) //set the singleton
        {
            Instance = this; 
        }
        else
        {
            enabled = false;
        }
        tooltipText = GetComponentInChildren<Text>(); //once singelton set, find the text, only one text component for tool tip text
    }

    void Start()
    {
        Toggle(); //Tooltip to not show on start
    }

    void Update()
    {
        if(timer > -1)
        {
            timer += Time.deltaTime;
            if(timer > tooltipTime)
            {
                Toggle();
            }
        }
    }

    public void Toggle(string message = null) 
    {
    if(message != null)
        {
            tooltipText.text = message; //set message to tool tip text
            gameObject.SetActive(true); //turn gameObject on
            timer = 0; //set timer counting
        }
        else
        {
            tooltipText.text = null; //set message to null, get rid of tool tip text
            gameObject.SetActive(false); //set gameObject off
            timer = -1; //set timer off
        }
    }
}
