using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using UnityEngine.SceneManagement;
using UnityEngine.AI; //for NavMesh

public class Ship : MonoBehaviour, IShip
{
    [TextArea]
    public string tooltipText;
    public Vector3 playerSpawnOffset;
    public Bounds boundBox; //public Bounds called boundBox
    public ShipWait waitState; //public class in a MonoBehaviour, ship wait not in inspector, Unity unable to serialize (put in a readable format) classes that don't inherit from MonoBehaviour
    public ShipMove moveState;

    public StateMachine StateMachine { get; private set; }
    public NavMeshAgent Agent { get; private set; }

    /// <summary>
    /// Finite State machine using ship wait state and move state
    /// </summary>
    private void Awake()
    {
        Agent = GetComponent<NavMeshAgent>();
        waitState = new ShipWait(this) { waitTime = waitState.waitTime }; //redo states, need to pass the Ship to the States
        moveState = new ShipMove(this); //can use this, is written within Ship class, refers an instance of Ship
        StateMachine = new StateMachine(waitState); //instantiate StateMachine and pass in the initial state (waitState)
    }
    /// <summary>
    /// Adds ship to map
    /// </summary>
    void Start()
    {
        PlayerController.Instance.MoveToPosition(transform.position + playerSpawnOffset); //player controller runs every frame
        WorldMap.Instance.AddInteraction(gameObject); //ship appears on map
    }

    private void Update()
    {
        StateMachine.OnUpdate(); //make sure StateMachine executes every Update
    }

    public string Inspect()
    {
        return tooltipText;
    }

    public void Leave()
    {
        if(GameManager.Instance.TargetFound == true)
        {
            MouseLook.Instance.CursorToggle = true;
            SceneManager.LoadScene(0);
        }
        else
        { 
            //Using string interpolation $ text and "'{place snippet of code}'"
            string message = ($"You need to find the target fish '{GameManager.Instance.targetText}' before you can leave!");
            Tooltip.Instance.Toggle(message);
        }
    }

    public void OnDrawGizmos() //OnDrawGizmos is an editor function used for debugging
    {
        Gizmos.DrawWireCube(boundBox.center, boundBox.size); //draw gizmo cube, size of boundBox is double the extents
        if(StateMachine != null && StateMachine.CurrentState != null) //check there is a CurrentState for the StateMachine and it is not equal to null
        {
            StateMachine.GetCurrentStateAsType<ShipState>().DrawGizmos(); //call DrawGizmos function
        }
    }
}

public abstract class ShipState : IState //Shipstate inherits from IState
{
    public Ship Instance { get; private set; }

    public ShipState(Ship instance)
    {
        Instance = instance;
    }

   
    public virtual void OnEnter() //add virtual to override OnEnter
    {
    }
    public virtual void OnExit() { } //add virtual to override OnExit
    public virtual void OnUpdate() { } //add vitural to override OnUpdate
    public virtual void DrawGizmos() { } //add virtual to override Draw Gizmos
}

/// <summary>
/// Ship Wait State
/// </summary>
[System.Serializable] //Serializable (put into readable format) will show WaitState in the Unity inspector
public class ShipWait : ShipState //create new class ShipWait which is a ShipState
{
    public Vector2 waitTime = new Vector2(1, 3);

    private float time = 0; //time
    private float timer = -1; //timer
    public ShipWait(Ship instance) : base(instance) //able to generate a constructor
    {
    }

    public override void OnEnter() //override is a form of polymorphism
    {
        time = Random.Range(waitTime.x, waitTime.y); //time is equal to a random time set in inspector eg. between 1 and 3
        timer = 0;
    }

    public override void OnUpdate() //override OnUpdate
    {
        if (timer > -1) //while state is running check if timer is > -1 means timer is ticking
        {
            timer += Time.deltaTime; //if the timer is > -1 it is ticking and needs to keep ticking it
            if (timer > time) //check is the timer is greater than time
            {
                Instance.StateMachine.SetState(Instance.moveState); //if the timer is greater than time refer to StateMachine set state to move
            }
        }
    }

    public override void OnExit() //override OnExit method
    {
        timer = -1; //when leaving the state set timer to -1 so timer is off.
    }
}
/// <summary>
/// Ship Move State
/// </summary>
[System.Serializable]

public class ShipMove : ShipState //able to generate the constructor for ShipMove
{
    private Vector3 targetPos; //variable stores target position as a Vector 3 (0,0,0 set of coordinates)

    public ShipMove(Ship instance) : base(instance)
    {
    }

    public override void OnEnter()
    {
        targetPos = GetRandomPosInBounds(); //get random target position in boundBox
        Instance.Agent.SetDestination(targetPos);
    }

    public override void OnUpdate()
    {
        if (Vector3.Distance(Instance.transform.position, targetPos) < Instance.Agent.stoppingDistance) //check distance between own position and target postion, needs to less than the agents stopping distance
        {
            Instance.StateMachine.SetState(Instance.waitState); //use Instance to access the ship
        }
    }
    
    public override void DrawGizmos() //Use gizmos for debugging
    {
        Gizmos.color = Color.red; //set gizmo to red
        Gizmos.DrawSphere(targetPos, 1); //draw gizmo at target position variable
    }

    public Vector3 GetRandomPosInBounds() //need to get three coordinates for a Vector 3
    {
        return new Vector3(Random.Range(-Instance.boundBox.extents.x, Instance.boundBox.extents.x), //x value, with a Bound Box the size is double the extents (extents is from the centre)
            Instance.transform.position.y, //y value
            Random.Range(-Instance.boundBox.extents.z, Instance.boundBox.extents.z)); //z value
    }
}