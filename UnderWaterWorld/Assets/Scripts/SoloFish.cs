using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoloFish : Fish //Fish inherits from IFauna
{
    public Bounds boundBox; //public variable of type Bounds
    public float smoothDamp;
    public Vector3[] collisionRays; //Vector3 array
    private float boundsDistance; //CHECK PUBLIC?
    public float obstacleDistance;
    public float boundsWeight;
    public float obstacleWeight;
    public float targetWeight;
    public float speed;
    public LayerMask obstacleMask;

    private Vector3 currentVelocity;
    //private Vector3 currentAvoidance;
    private Vector3 targetPosition;

    void Update()
    {
        if(Random.Range(0, 1000) < 10) //chance of executing, eg. 1 in 10 chance
        {
            targetPosition = GetRandomPosInBounds(); //randomly reposition the target the fish is swimming towards
        }
        MoveStep();
    }
    /// <summary>
    /// Render gizmos for visual debugging
    /// </summary>
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(boundBox.center, boundBox.size);
        Gizmos.DrawWireSphere(targetPosition, 1); //1 whole unit for size
    }
    /// <summary>
    /// Randomly spawn position of Solo Fish within the boundBox
    /// </summary>
    /// <returns></returns>
    private  Vector3 GetRandomPosInBounds()
    {
        Vector3 randomCoords = new Vector3(Random.Range(-boundBox.extents.x, boundBox.extents.x),
            Random.Range(-boundBox.extents.y, boundBox.extents.y),
            Random.Range(-boundBox.extents.y, boundBox.extents.z));
        return boundBox.center + randomCoords;
    }

    private void MoveStep() //Called MoveStep as it is a frame by frame operation, stepping every frame
    {
        Vector3 bounds = Bounds() * boundsWeight; //local variable set to bounds and set to what Bounds method returns
        Vector3 avoidance = ObstacleAvoidance() * obstacleWeight;
        Vector3 target = (targetPosition - transform.position).normalized * targetWeight;
        Vector3 motion = bounds + avoidance + target;
        motion = Vector3.SmoothDamp(transform.forward, motion, ref currentVelocity, smoothDamp);
        motion = motion.normalized * speed;
        if(motion == Vector3.zero)
        {
            motion = transform.forward;
        }
        transform.forward = motion;
        transform.position += motion * Time.deltaTime;
    }

    private Vector3 ObstacleAvoidance()
    {
        Vector3 vector = Vector3.zero; //local Vector 3 set to Vector3.zero same as Vector3(0,0,0)
        float largestDistance = int.MinValue; //check through all collision rays

        for (int i = 0; i < collisionRays.Length; i++) //for loop iterates through the collision rays and increments
        {
            Vector3 currentDir = transform.TransformDirection(collisionRays[i].normalized); //normalized turns all values into a values between 0 and 1.
            if(Physics.Raycast(transform.position, currentDir, out RaycastHit hit, obstacleDistance, obstacleMask) == true) //use Physics raycast, start raycasting from own position, store what is hit in raycast variable, obstacleMask last parameter
            {
                Debug.DrawRay(transform.position, currentDir * obstacleDistance, Color.green); //if the statement executes knows that something has been hit, turns green
                float sqrDistance = (hit.point - transform.position).sqrMagnitude; //check distance using sqrDistance (faster)
                if(sqrDistance > largestDistance) //if square Distance is greater than largest Distance
                {
                    largestDistance = sqrDistance; //then set largest Distance to square Distance
                    vector += -currentDir; //minus current direction, want the direction going to the fish
                }
            }
            else
            {
                Debug.DrawRay(transform.position, currentDir * obstacleDistance, Color.red);
                vector += currentDir; //adding the inverse of the direction to the thing hit
            }
        }
        return vector.normalized;
    }
  
    private Vector3 Bounds() //steers solo fish towards the boundBox if outside
    {
        Vector3 centerOffset = boundBox.center - transform.position; //gets direction to the boundBox centre from the position of the fish
        bool withinBounds = (centerOffset.magnitude >= boundsDistance * 0.9); //boolean within bounds, 0.9 if greater than 90% of the bounds, buffer zone stops fish from getting too close to the edge.
        if(withinBounds == true) //if the magnitude (length of direction is greater than the distance) is true
        {
            return centerOffset.normalized; //MoveStep uses normalized values to calculate direction
        }
        return Vector3.zero; //if statement does not execute return zero and fish within bounds
    }
}