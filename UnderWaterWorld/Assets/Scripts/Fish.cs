using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Fish : MonoBehaviour, IFauna //abstract class
{
    [TextArea]
    public string tooltipText;

    protected Transform swimTarget;

    public string Inspect()
    {
        return tooltipText;
    }

    public void Photograph(string folderPath = null)
    {
        if(folderPath != null)
        {
            string date = System.DateTime.Now.Day.ToString() + System.DateTime.Now.Month.ToString() + System.DateTime.Now.TimeOfDay.ToString().Replace(':', '-');
            ScreenCapture.CaptureScreenshot(folderPath + $"/UWW_{date}.png", 2); //$ uses string interpolation and curly brackets instead of whole string date above
            //
        }
      
        if(GameManager.Instance.TargetFound == false && (Object)GameManager.Target == this)
        {
            GameManager.Instance.TargetFound = true;
            //Trigger end cut scene here
        }
    }

    public void SwimAroundTarget(Transform target)
    {
        if (swimTarget != target)
        {
            swimTarget = target;
        }
    }
}
