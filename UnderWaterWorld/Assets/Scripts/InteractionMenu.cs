using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class InteractionMenu : MonoBehaviour
{
    public Image optionA; //optionA variable
    public Image optionB; //optionB varaible, could place on one line
    public Text optionBText;

    private IInteraction currentInteraction;
    private int optionIndex = -1;
    private float timer = -1; //timer needed for changes, otherwise controller goes crazy

    public static InteractionMenu Instance { get; private set; } //declare singleton

    private void Awake()
    {
        if(Instance == null) //singleton initialization
        {
            Instance = this;
        }
        else
        {
            enabled = false;
        }
        if(Directory.Exists(Application.dataPath + "/Resources/PhotoGallery") == false) //if the game loads, able to go straight into game scene without loading gallery from main menu, menu also makes photo gallery folder construction, added here to make debugging easier
        {
            Directory.CreateDirectory(Application.dataPath + "/Resources/PhotoGallery"); //create directory for photo gallery if false above
        }
    }

    void Start()
    {
        Deactivate(); //when game starts menu is deactivated, menu turns off when game starts
    }

    void Update()
    {
        if(gameObject.activeSelf == true && timer == -1)
        {
            int axis = Mathf.FloorToInt(Input.GetAxis("Menu Selection"));
            if(axis != 0) //if plus 1 means pressing a positive input if 0 pressing a negative input
            {
                timer = 0;
                optionIndex += axis;
                if(optionIndex < 0)
                {
                    optionIndex = 1;
                }
                else if(optionIndex > 1)
                {
                    optionIndex = 0;
                }
                UpdateUISelection();
            }
        }

        if(timer > -1) //know that timer is counting
        {
            timer += Time.deltaTime; //increment timer
            if(timer > 0.25f) //0.25 secpmd input buffer
            {
                timer = -1; //timer has expired
            }
        }
    }

    void UpdateUISelection() //private function
    {
        switch (optionIndex)
        {
            case 0:                         //define cases for switch
                optionA.color = Color.white; 
                optionB.color = Color.grey; //greys out option which isn't selected
                break;
            case 1:
                optionA.color = Color.grey;
                optionB.color = Color.white; //greys out option which isn't selected
                break;
        }         
    }

    void Photograph(IFauna fauna) //private function, fauna passed to be compared to objective later in game
    {
        string gallery = Application.dataPath + "/Resources/PhotoGallery";
        DirectoryInfo dir = new DirectoryInfo(gallery); //getting the properties of the gallery folder and storing in the DirectoryInfo, needed to count the amount of files inside the folder
        FileInfo[] files = dir.GetFiles("*.png"); //array has all images files stored in photo gallery
        
        int count = optionIndex; //counter variable
        foreach (FileInfo info in files) //loop through files to check
        { 
            if (info.Name.Contains("UWW_") == true) //check if file contains UWW prefix
            {
                count++;                //if it is true (contains UWW) screenshot has been found and count increases
            }
        }

        if(count < 30)                  //max limit of photos in gallery is 30
        {
            fauna.Photograph(gallery);  //pass fauna to compare to objective or target fish, gallery stores directory name
        }
        else                            //if count is greater than or equal to 30 
        {
            fauna.Photograph();         //pass fauna to compare to objective or target fish, takes a photo but no screenshot file created or stored
        }
    }
    //can pass any data type as InteractionType but constraint at the end of line (using where) only lets the type IInteration
    //out sets up a variable that then can be returned, able to use whatever returns outside of the method
    bool GetInteractionAsType<InteractionType>(out InteractionType target) where InteractionType : IInteraction //generic method GetInteractionAsType, requires chevron brackets before parentheses as it is a generic method, constraint placed at end using where IIteraction Type: IInteraction
    {
        if(currentInteraction is InteractionType interaction) //converts currentInteraction as an interaction, way of casting a dataype as a generic method
        {
            target = interaction;
            return true; 
        }
        target = default; //otherwise set target to the default
        return false; //return false
    }
    //define functions
    public void Activate(IInteraction interaction) //activate takes parameter IInteraction called interaction
    {
        currentInteraction = interaction;       //activate interaction menu
        if (GetInteractionAsType(out IFauna fauna) == true)
        {
            optionBText.text = "Photograph";    //if the interaction is fauna take a photograph
            optionBText.color = Color.black; 
        }
        else if (GetInteractionAsType(out IKey key) == true)
        {
            optionBText.text = "Pick up";       //if the interaction is a key pick up
            optionBText.color = Color.black;
        }
        else if (GetInteractionAsType(out IGate gate) == true)
        {
            optionBText.text = "Unlock";        //if the interaction is a gate
            optionBText.color = Color.black;
        }
        else if (GetInteractionAsType(out IShip ship) == true)
        {
            optionBText.text = "Head home";     //if the interaction is a ship
            optionBText.color = Color.black;
        }
        optionIndex = 0;
        UpdateUISelection();                    //update UI selection
        gameObject.SetActive(true);
    }

    public void Deactivate() //deactivate has no parameters
    {
        if(optionIndex == 0)
        {
            Tooltip.Instance.Toggle(currentInteraction.Inspect());  //pass current interaction information to tooltip
        }
        else if(optionIndex == 1)
        {
            if(GetInteractionAsType(out IFauna fauna) == true)      //if able to convert the interaction into a fauna, then check what type of interaction is currently being interacted with
            {
                Photograph(fauna);                                  //Photograph method with fauna passed through
            }
            else if(GetInteractionAsType(out IKey key) == true)     //if fauna doesn't execute then try another relevant interaction type, stores variable in temporary key variable
            {
                key.Pickup();                                       //through key variable access Pickup method 
            }
            else if(GetInteractionAsType(out IGate gate) == true)   //if key doesn't execute then try another relevant interaction type
            {
                gate.Unlock();                                      //through gate variable access Unlock method
            }
            else if(GetInteractionAsType(out IShip ship) == true)   //if gate doesn't execute then try another relevant interaction type
            {
                ship.Leave();                                       //call leave function from the ship
            }
        }
        gameObject.SetActive(false);                                //turns interaction menu off
        optionIndex = -1;
        currentInteraction = null;
    }
}