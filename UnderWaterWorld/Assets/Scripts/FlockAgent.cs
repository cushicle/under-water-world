using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlockAgent : Fish //Fish inherits from IFauna
{
    public float viewAngle;
    public float smoothDamp; //vector is smoothed
    public LayerMask obstacleMask;
    public Vector3[] collisionRays; //Vector 3 array collisionRays

    private float speed;
    private Vector3 velocity;
    private Vector3 currentAvoidance;
    private FlockManager flockInstance;
    private List<FlockAgent> cohesionNeighbours = new List<FlockAgent>();
    private List<FlockAgent> separationNeighbours = new List<FlockAgent>();
    private List<FlockAgent> alignmentNeighbours = new List<FlockAgent>();

    /// <summary>
    /// Initializes the agent with passed flock and speed
    /// </summary>
    /// <param name="manager"></param>
    /// <param name="intiSpeed"></param>
    public void Initialize(FlockManager manager, float initSpeed)
    {
        flockInstance = manager;
        speed = initSpeed;
    }

    /// <summary>
    /// This method calculates and applies movement for the agent on a frame by frame basis.
    /// </summary>
    public void Move()
    {
        FindNeighbours();
        CalculateSpeed();

        Vector3 cohesion = Cohese() * flockInstance.cohesion; //cohesion vector3, fish instructed to moved toward centre of flock
        Vector3 separation = Separate() * flockInstance.separation; //separation vector3, fish avoid collision using a min distance from neighbours (true is fish position is within view radius of neighbour)
        Vector3 alignment = Align() * flockInstance.alignment; //alignment vector3, average direction of all fish in the flock, fish move in the same direction as the flock
        Vector3 bounds = Bounds() * flockInstance.bounds; //Bound vector keeps fish agents close to flock agent
        Vector3 avoidance = ObstacleAvoidance() * flockInstance.obstacleAvoidance; //generates an ObstacleAvoidance vector on a frame by frame basis

        Vector3 motion = cohesion + separation + alignment + bounds + avoidance; //overall motion of fish agent
        motion = Vector3.SmoothDamp(transform.forward, motion, ref velocity, smoothDamp); //smooth motion of fish using SmoothDamp
        motion = motion.normalized * speed; //normalize the motion, a vector is either a point or a direction, if a direction then it has a length, normalized used to get direction, vector keeps same direction but is set to 1 or 0

        if (motion == Vector3.zero)
        {
            motion = transform.forward;
        }
        transform.forward = motion; //redirect orientation
        transform.position += motion * Time.deltaTime; //move position
    }
    /// <summary>
    /// Get the neighbours for cohesion, alignment, and separation.
    /// </summary>
    private void FindNeighbours()
    {
        cohesionNeighbours.Clear(); //Clear removes elements from list
        separationNeighbours.Clear();
        alignmentNeighbours.Clear();

        for (int i = 0; i < flockInstance.SpawnedAgents.Length; i++) //for loop iterates through the spawned flockInstances and increments
        {
            FlockAgent agent = flockInstance.SpawnedAgents[i];
            if (agent != this) //if the agent is not equal to FlockAgent
            {
                float distanceSqr = Vector3.SqrMagnitude(agent.transform.position - transform.position); //check distance using sqrDistance (faster)
                if (distanceSqr <= flockInstance.cohesionDistance * flockInstance.cohesionDistance) //distanceSqr equal to less than the cohesionDistance multiplied by cohesionDistance
                {
                    cohesionNeighbours.Add(agent); //Add agent to end of list
                }
                if (distanceSqr <= flockInstance.separationDistance * flockInstance.separationDistance)
                {
                    separationNeighbours.Add(agent);
                }
                if (distanceSqr <= flockInstance.alignmentDistance * flockInstance.alignmentDistance)
                {
                    alignmentNeighbours.Add(agent);
                }
            }

        }
    }

    /// <summary>
    /// Calculate average speed of agent based on neighbours
    /// </summary>
    private void CalculateSpeed()
    {
        if (cohesionNeighbours.Count == 0)
        {
            return;
        }
        speed = 0;
        for (int i = 0; i < cohesionNeighbours.Count; i++)
        {
            speed += cohesionNeighbours[i].speed;
        }
        speed /= cohesionNeighbours.Count; //average speed of agent based on neighbours
        speed = Mathf.Clamp(speed, flockInstance.speed.x, flockInstance.speed.y); //Math function (Mathf), clamps the given value between the givien min float and max float, return value is within mix max range.
    }

    /// <summary>
    /// Generates the cohesion vector on a frame by frame basis.
    /// </summary>
    /// <returns></returns>
    private Vector3 Cohese()
    {
        Vector3 vector = Vector3.zero; //cohesion vector
        int neighboursInView = 0; //local variable neighboursInView
        if (cohesionNeighbours.Count == 0)
        {
            return vector;
        }
        for (int i = 0; i < cohesionNeighbours.Count; i++)
        {
            if(VectorInView(cohesionNeighbours[i].transform.position) == true)
            {
                neighboursInView++;
                vector += cohesionNeighbours[i].transform.position;
            }
        }
        if(neighboursInView == 0)
        {
            return vector;
        }
        vector /= neighboursInView;
        vector -= transform.position;
        return vector.normalized;
    }
    /// <summary>
    /// Return true if the given position is within agents view radius
    /// </summary>
    /// <param name="position"></param>
    /// <returns></returns>
    
    private Vector3 Separate()
    {
        Vector3 vector = transform.forward;
        if(separationNeighbours.Count == 0)
        {
            return vector;
        }
        int neighboursInView = 0;
        for(int i = 0; i < separationNeighbours.Count; i++)
        {
            if(VectorInView(separationNeighbours[i].transform.position) == true)
            {
                neighboursInView++;
                vector += transform.position - separationNeighbours[i].transform.position;
            }
        }
        vector /= neighboursInView;
        return vector.normalized;
    }

    /// <summary>
    /// Generate the alignment vector
    /// </summary>
    /// <returns></returns>

    private Vector3 Align()
    {
        Vector3 vector = transform.forward;
        if (alignmentNeighbours.Count == 0)
        {
            return vector;
        }
        int neighboursInView = 0;
        for (int i = 0; i < alignmentNeighbours.Count; i++)
        {
            if (VectorInView(alignmentNeighbours[i].transform.position) == true)
            {
                neighboursInView++;
                vector += alignmentNeighbours[i].transform.position;
            }
        }
        vector /= neighboursInView;
        return vector.normalized; //when normalized a vector keeps the same direction but its length is 1.0.
    }

    /// <summary>
    ///Generate vector to keep the agent close to the flock agent 
    /// </summary>
    /// <returns></returns>
    private Vector3 Bounds()
    {
        Vector3 centerOffset = flockInstance.transform.position - transform.position;
        bool withinBounds = (centerOffset.magnitude >= flockInstance.boundsDistance * 0.9); //within 90 percent of the bounds, magnitude similar to distance eg length of a line
        if(withinBounds == true)
        {
            return centerOffset.normalized;
        }
        return Vector3.zero;
    }

    /// <summary>
    /// Generate an obstacle avoidance vector on a frame by frame basis
    /// </summary>
    /// <returns></returns>
    private Vector3 ObstacleAvoidance()
    {
        Vector3 vector = Vector3.zero;
        if (Physics.Raycast(transform.position, transform.forward, out RaycastHit hit, flockInstance.obstacleDistance, obstacleMask) == true)
        {
            vector = AvoidObstacle();
        }
        else
        {
            currentAvoidance = Vector3.zero; //shorthand for writing Vector3(0,0,0), elements equal to 0.
        }
        return vector;
    }

    /// <summary>
    /// Checks all collision rays for collision
    /// Returns the most suitable direction to move in
    /// </summary>
    /// <returns></returns>
    private Vector3 AvoidObstacle()
    {
        if (currentAvoidance != Vector3.zero)
        {
            if (Physics.Raycast(transform.position, transform.forward, out RaycastHit hit, flockInstance.obstacleDistance, obstacleMask) == false)
            {
                return currentAvoidance;
            }
        }
        float maxDist = int.MinValue;
        Vector3 dir = Vector3.zero;
        for(int i = 0; i < collisionRays.Length; i++)
        {
            Vector3 currentDir = transform.TransformDirection(collisionRays[i].normalized);
            if(Physics.Raycast(transform.position, currentDir, out RaycastHit hit, flockInstance.obstacleDistance, obstacleMask) == true)
            {
                float sqrDist = (hit.point = transform.position).sqrMagnitude;
                if(sqrDist > maxDist)
                {
                    maxDist = sqrDist;
                    dir = currentDir;
                }
            }
            else
            {
                dir = currentDir;
                currentAvoidance = currentDir.normalized;
                return dir.normalized;
            }
        }
        return dir.normalized; //local variable Vector3, normalized
    }
    private bool VectorInView(Vector3 position)
    {
        return Vector3.Angle(transform.forward, position - transform.position) <= viewAngle;
    }
}