using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [Header("Spawn settings")]
    public List<GameObject> fishPrefabs;
    public List<Transform> zoneASpawns;
    public List<Transform> zoneBSpawns;
    public List<Transform> zoneCSpawns;
    [Header("UI")]
    public Text targetText;
    [Header("Cutscenes")]
    public Camera cutSceneCamera;
    public Camera playerCamera;
    public Vector3 cameraOffset;
    public PlayableDirector cutSceneDirector;
    public PlayableAsset introCutScene;
    public PlayableAsset targetFishCutScene; // End cutscene

    public static GameManager Instance { get; private set; } //singleton

    public static IFauna Target { get; set; } //static to access from class type, publicly read priavtely set

    public bool TargetFound { get; set; } = false;
    #region  Cut Scene Properties
    public bool OrbitCameraTarget { get; set; } = false;

    public bool TargetFoundCutscene { get; set; } = false;

    public Transform CameraTarget { get; private set; }
    #endregion
    public void Awake()
    {
        if(Instance == null) //set singleton
        {
            Instance = this;
        }
        else
        {
            enabled = false; //disables, only need one game manager active in the game at a time
        }

        #region Spawn Target Fish
        GameObject targetFish = fishPrefabs[Random.Range(0, fishPrefabs.Count)]; //flocking fish (flock agent) never target fish
        while(targetFish.GetComponent<IFauna>() == null) //loop checks IFauna to get a target fish
        {
            targetFish = fishPrefabs[Random.Range(0, fishPrefabs.Count)];
        }
        fishPrefabs.Remove(targetFish); //remove target fish
        Transform spawn = zoneCSpawns[Random.Range(0, zoneCSpawns.Count)]; //target fish first fish spawned in to exclude from the rest of the list afterwards, spawned into zone C.
        GameObject spawnedFish = Instantiate(targetFish, spawn.transform.position, spawn.transform.rotation); //track spawned fish
        if (spawnedFish.TryGetComponent(out SoloFish sFish) == true)
        {
            sFish.boundBox.center = spawn.position;
        }
        spawnedFish.name = targetFish.name;
        targetText.text = targetFish.name; //set text component of target text
        Target = spawnedFish.GetComponent<IFauna>(); //the spawned instance of the target fish (target fish is of the type IFauna)
        zoneCSpawns.Remove(spawn); //remove spawns from zone C
        #endregion

        if(fishPrefabs.Count > 0 && zoneASpawns.Count > 0) //spawn in zone A
        {
            SpawnZone(zoneASpawns);
        }
        if (fishPrefabs.Count > 0 && zoneBSpawns.Count > 0) //spawn in zone B
        {
            SpawnZone(zoneBSpawns);
        }
        if (fishPrefabs.Count > 0 && zoneCSpawns.Count > 0) //spawn in zone C, all if statements no else statements, all will be checked
        {
            SpawnZone(zoneCSpawns);
        }
    }

    private void Start()
    {
        TriggerCutscene(introCutScene);
    }

    // Update is called once per frame
    void Update()
    {
        if(TargetFound == true && TargetFoundCutscene == false)
        {
            Debug.Log(OrbitCameraTarget); //Orbit camera function
            TriggerCutscene(targetFishCutScene);
            TargetFoundCutscene = true;
        }


        if(CameraTarget != null) //if there is target
        {
            if(OrbitCameraTarget == true) //if there is a target and the boolean is true
            {
                cutSceneCamera.transform.RotateAround(CameraTarget.position, CameraTarget.up, 5 * Time.deltaTime); //using transform class RotateAround camera target, rotate around the y axis(up), time hard coded.
                cutSceneCamera.transform.LookAt(CameraTarget); //using transform class LookAt the CameraTarget
            }
        }
    }

    void SpawnZone(List<Transform> spawnList) //cycles through fish prefabs, takes parameter called spawnList, pass a list of transforms to method
    {
        int zoneCount = spawnList.Count; //integer, used for optimisation only need to iterate through once
        for(int i = 0; i < zoneCount; i++) //for loop to iterate through all spawns in the list, called for each spawn list eg. spawn list A, B & C.
        {
            int f = Random.Range(0, fishPrefabs.Count); //f is fish index, cycles through fish prefabs until no more fish prefabs or no more zones
            Transform spawn = spawnList[Random.Range(0, spawnList.Count)]; //temp local variable current spawn, generate random index, gives random spawn (hard coded can't use zone count)
            GameObject fish = Instantiate(fishPrefabs[f], spawn.transform.position, spawn.transform.rotation); //local variable GameObject called fish, pass in spawn points
            if (fish.TryGetComponent(out SoloFish sFish) == true)
            {
                sFish.boundBox.center = spawn.position; //set flock bounds centre to 0,0,0
            }
            fish.name = fishPrefabs[f].name; //rename fish, why? when using instantiates flock plus(clone) added to list. To avoid target fish text labelling using the word clone.
            fishPrefabs.Remove(fishPrefabs[f]); //remove fish prefabs spawned in from list to stop spawning in again
            spawnList.Remove(spawn); //remove spawn list 
        }
    }

    public void TogglePlayer(bool toggle) //dry principle, don't repeat yourself! use the boolean directly to half the amount of code
    {
        PlayerController.Instance.enabled = toggle; //if toggle is equal to true turn on, if toggle equal to false turn off (instead of using false)
        MouseLook.Instance.gameObject.SetActive(toggle);
        playerCamera.gameObject.SetActive(true);
        Interaction.Instance.enabled = toggle;
        Actions.Instance.enabled = toggle;
    }

    public void PositionCinematicCamera(Transform target) //takes Transform as a parameter
    {
        CameraTarget = target; //set camera target
        if(target != null) //check if not equal to null incase it is null and desets the camera
        {
            cutSceneCamera.transform.position = target.position + cameraOffset; 
        }
    }

    public void TriggerCutscene(PlayableAsset cutScene) 
    {
        if (cutSceneDirector != null && cutScene != null)
        {
            StartCoroutine(PlayCutScene(cutScene)); //using Coroutine so cutscene can return to the end of the frame
        }
    }

    private IEnumerator PlayCutScene(PlayableAsset cutScene)
    {
        TogglePlayer(false); //toggle player and pass as false to turn player off
        cutSceneCamera.gameObject.SetActive(true);
        cutSceneDirector.playableAsset = cutScene; //playable asset variable and set equal to cutscene
        cutSceneDirector.Play(); //play cutscene through the director
        while(cutSceneDirector.time != cutSceneDirector.playableAsset.duration) //while loop director has reached the last frame of cutscene, knows cutscene finished playing
        {
            yield return new WaitForEndOfFrame();
        }
        cutSceneDirector.Stop(); //stop cutscene
        cutSceneCamera.gameObject.SetActive(false); //turn camera off as a gameObject
        TogglePlayer(true); //toggle player back on
    }

    public void StopPlayCutScene()
    {
        TargetFoundCutscene = true;
    }
}
